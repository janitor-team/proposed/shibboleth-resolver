/* src/shibresolver/config_pub.h.  Generated from config_pub.h.in by configure.  */
/* if you have the gssapi libraries */
#define SHIBRESOLVER_HAVE_GSSAPI 1

/* if you have the GNU gssapi libraries */
/* #undef SHIBRESOLVER_HAVE_GSSGNU */

/* if you have the Heimdal gssapi libraries */
/* #undef SHIBRESOLVER_HAVE_GSSHEIMDAL */

/* if you have the MIT gssapi libraries */
#define SHIBRESOLVER_HAVE_GSSMIT 1

/* if you have GSS naming extensions support */
#define SHIBRESOLVER_HAVE_GSSAPI_NAMINGEXTS 1
